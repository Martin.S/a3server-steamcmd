#!/bin/bash
start_time=$(date +%s) ;
cd /home/steam/arma3/steamapps/workshop/content/107410 ; 
start_time=$(date +%s) ;
echo "Running rename command, this will take a few minutes." ; 
echo "Script executed in: ${PWD}" ; 
find . -depth -exec rename 's/(.*)\/([^\/]*)/$1\/\L$2/' {} \; ; 
finish_time=$(date +%s) ;
elapsed_time=$((finish_time  - start_time)) ;
((sec=elapsed_time%60, elapsed_time/=60, min=elapsed_time%60, hrs=elapsed_time/60)) ;
timestamp=$(printf "Total time taken - %d hours, %d minutes, and %d seconds." $hrs $min $sec) ;
echo "done!"; 
cd -;