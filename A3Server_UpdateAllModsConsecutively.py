from subprocess import Popen
from os import listdir
from sys import argv

if len(argv) == 2:
    # Get input from command line.
    user = argv[1]
else:
    print ("Invalid Args")
    raise ValueError('Value error, enter steam user')


path = "/home/steam/arma3/steamapps/workshop/content/107410"
mods = listdir(path)
#newmods = ["@"+ x for x in mods]
modString = ",".join(mods)


#Initial App update
shell = f'steamcmd +login {user} +force_install_dir /home/steam/arma3/ +app_update "233780" -beta creatordlc'
p0 = Popen(shell, shell=True)
p0.wait()

# Run modlist update
for fileID in mods:
    shell = f'+force_install_dir /home/steam/arma3/ +workshop_download_item 107410 {fileID} validate'
    shell += "+quit"
    p1 = Popen(shell, shell=True)
    p1_stdout = p1.stdout.read().decode('utf-8')
    p1.wait()

    #Error handling
    if "ERROR! Download item" in p1_stdout:
        #Recursivly loop 3 times, on the 3rd fail add it to stderr
        str = "test"



# Run a3server_createsymlinks.py
p2 = Popen(["python3", "a3server_createsymlinks.py"])
p2.wait()

# Run renameFiles.sh
p3 = Popen(["bash", "arma3linux_renamer.sh"])
p3.wait()

print("Update Complete")
