from subprocess import Popen
from os import listdir
from sys import argv

if len(argv) == 2:
    # Get input from command line.
    user = argv[1]
else:
    print ("Invalid Args")
    raise ValueError('Value error, enter steam user')


path = "/home/steam/arma3/steamapps/workshop/content/107410"
mods = listdir(path)
#newmods = ["@"+ x for x in mods]
modString = ",".join(mods)



shell = 'steamcmd +login ' + user + ' +force_install_dir /home/steam/arma3/ +app_update "233780" -beta creatordlc '

for fileID in mods:
    shell += '+workshop_download_item 107410 ' + str(fileID) + ' validate '

shell += " +quit"


# Run modlist update
p1 = Popen(shell, shell=True)
p1.wait()

# Run a3server_createsymlinks.py
p2 = Popen(["python3", "a3server_createsymlinks.py"])
p2.wait()

# Run renameFiles.sh
p3 = Popen(["bash", "arma3linux_renamer.sh"])
p3.wait()

print("Update Complete")
