# -*- coding: utf-8 -*-
"""
Created on Sat Jun 17 18:48:18 2023

@author: RedBull
"""
import os
import sys
from time import sleep

os.chdir(os.path.dirname(__file__))

if len(sys.argv) == 2:
    # Get input from command line.
    modlist = sys.argv[1]
else:
    print ("Invalid Args")
    raise ValueError('Value error, enter modlist')


#Get mods
file = open(modlist,mode='r')
mods = file.read()
file.close()

#Run server then spawn 3 headless clients
shell_cmd = "taskset -c 0 screen -L -Logfile Log_Server.log -d -m -S arma3server /home/steam/arma3/arma3server -name=TheFewBadMen -config=server.cfg -port=2302 " + mods
os.system(f"cd /home/steam/arma3/ && {shell_cmd}")
print("Server started")
sleep(5)


for cpu_core in range(1, 4): # Loop through physical CPU cores 1, 2, and 3
    shell_cmd = f"taskset -c {cpu_core} screen -L -Logfile Log_HC_{cpu_core}.log -d -m -S arma3headless_{cpu_core} /home/steam/arma3/arma3server -client -connect=127.0.0.1 -port=2302 -password='FBM' {mods}"
    os.system(f"cd /home/steam/arma3/ && {shell_cmd}")
    print(f"Headless client {cpu_core} started")
    sleep(5)
