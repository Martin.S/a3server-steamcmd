# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 15:37:54 2023

@author: RedBull
"""
import os 
import sys
from bs4 import BeautifulSoup

os.chdir(os.path.dirname(__file__))

print (len(sys.argv) )
if len(sys.argv) == 3:
    # Get input from command line.
    user = sys.argv[1]
    HTMLfile = sys.argv[2]
else:
    print ("Invalid Args")
    raise ValueError('Value error, enter steam user and html file \n Example: python3 A3Server_UpdateModlistHTML.py "user" "modlist.html"')

with open(HTMLfile) as fp:
    soup = BeautifulSoup(fp, 'html.parser')

'''
mod_name_list = []
for DN in soup.find_all(attrs={"data-type": "DisplayName"}):
    data = DN.get_text()
    mod_name_list.append(data)
'''    
mod_id_list = []
for link in enumerate(soup.find_all('a')):
    href = link[1].get('href')
    steamID = href.split("?id=",1)[1]
    #steamID +=" #" +mod_name_list[link[0]]
    #print(steamID)
    mod_id_list.append(steamID)

if os.path.exists("modlist.txt"):
    file = open("modlist.txt", "w")
    file.write('-mod="');
    for modID in mod_id_list:
        file.write("@" + str(modID) + ";");
    file.write('"');
    file.close()

#Setup
shell = 'steamcmd +login ' + user + ' +force_install_dir /home/steam/arma3/ +app_update "233780" -beta creatordlc '

for fileID in mod_id_list:
    shell += '+workshop_download_item 107410 ' + str(fileID) + ' validate '

shell += " +quit"

#rint(shell)
os.system (shell)
