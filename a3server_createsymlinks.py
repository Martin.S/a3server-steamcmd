# -*- coding: utf-8 -*-
"""
Created on Sat Jun 17 01:52:24 2023

@author: RedBull
"""
import os

#Replace with args
sourceDir = "/home/steam/arma3/steamapps/workshop/content/107410/"
destDir = "/home/steam/arma3/"


#Find folders
addons = os.scandir(sourceDir)
for iterator in addons:
        print("Checking: " + str(iterator.name), end=" - ")
        if iterator.is_dir():
            symlinkStr = "@"+str(iterator.name)
            src = sourceDir+str(iterator.name)
            dst = destDir + symlinkStr
            if os.path.islink(dst):
                print ("link already exist")
            else:
                os.symlink(src, dst, True)
                print('Symlink created')